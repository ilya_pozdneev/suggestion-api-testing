### Overview ###

This repository provides three docker containers:

- Simple Suggestion API
- Newman API testing
- Unit tests for one of the classes

### Usage ###

./build.sh

docker-compose up