#! /usr/bin/python3
# coding: utf-8

from urllib.parse import quote_plus

class URLEncoder():
    _special_chars = '/'
    """
    URLEncoder class is meant for translating strings into appropriate
    url extensions
    In case of suggestion service, artist name is encoded so it can be
    used to query music map service
    """
    def __init__(self):
        """
        URLEncoder - basic encoder class to translate strings
        into url extensions, ex. 'Led Zeppelin' => 'led+zeppelin'
        """
        # these characters require special attention, ex. '/' in 'ac/dc'

    @property
    def special_chars(self) -> str:
        """
        Gets the list of special characters

        :return: Special Characters
        :rtype: str
        """
        return self._special_chars

    @classmethod
    def encode(self, str_to_encode):
        """
        Returns string that represents encoded url

        :param s: Python string
        :type: str
        :return: Encoded Url
        :rtype: str
        """
        safe = self._special_chars
        temp = quote_plus(str_to_encode.lower(), safe)
        for char in self._special_chars:
            if char in temp:
                temp = temp.replace('/', quote_plus(char.lower())).replace('%', '-')

        return temp
