# coding: utf-8

from url_encoder import URLEncoder
import unittest

class TestUrlEncoderMethods(unittest.TestCase):
    test_values = (('test', 'test'),
                   ('Test', 'test'),
                   ('TEST', 'test'),
                   ('test test', 'test+test'),
                   ('test/test', 'test-2Ftest'))

    def test_string_to_url(self):
        test = URLEncoder()
        for string, url in self.test_values:
            self.assertEqual(test.encode(string), url)

if __name__ == '__main__':
    unittest.main()
